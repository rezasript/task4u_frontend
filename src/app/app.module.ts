import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './template/header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MatButtonModule,
  MatIconModule,
  MatSelectModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatGridListModule,
  MatTooltipModule,
  MatRadioModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatChipsModule,
  MatAutocompleteModule,
  MatBadgeModule,
  MatSnackBarModule, MatListModule, MatCardModule, MatProgressBarModule, MatProgressSpinnerModule, MatRippleModule
} from '@angular/material';
import { FooterComponent } from './template/footer/footer.component';
import { SectionsComponent } from './search/sections/sections.component';
import { FormComponent } from './search/form/form.component';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NumbersOnlyDirective} from './directives/numbers-only.directive';
import { CountryPipe } from './search/form/pipes/country.pipe';
import { LanguagePipe } from './search/form/pipes/language.pipe';
import { MatchComponent } from './search/match/match.component';
import { RegisterComponent } from './register/register.component';
import { AppRoutingModule } from './app-routing.module';
import { SearchComponent } from './search/search/search.component';
import { LoginComponent } from './login/login.component';
import {CookieService} from 'ngx-cookie-service';
import { ProfileComponent } from './profile/profile.component';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import { HomeComponent } from './home/home.component';
import { HeaderUnsignedComponent } from './template/header/header-unsigned/header-unsigned.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SectionsComponent,
    FormComponent,
    NumbersOnlyDirective,
    CountryPipe,
    LanguagePipe,
    MatchComponent,
    RegisterComponent,
    SearchComponent,
    LoginComponent,
    ProfileComponent,
    HomeComponent,
    HeaderUnsignedComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatCheckboxModule,
    MatGridListModule,
    MatTooltipModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatSnackBarModule,
    FormsModule,
    ReactiveFormsModule,
    MatListModule,
    MatCardModule,
    MatProgressBarModule,
    AppRoutingModule,
    MatProgressSpinnerModule,
    MatRippleModule
  ],
  providers: [
    CookieService,
    SearchComponent,
    MatchComponent,
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { strict: true } }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
