import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[appNumbersOnly]'
})
export class NumbersOnlyDirective {

  constructor(private el: ElementRef) { }

  // Allow numbers only
  @HostListener('input') onInputChange() {
    this.el.nativeElement.value = this.el.nativeElement.value.replace(/[^0-9]*/g, '');
  }

}
