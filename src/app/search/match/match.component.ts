import {Component, Input, OnInit} from '@angular/core';
import {BackendService} from '../form/backend.service';
import {IbestMatch} from '../../interfaces/best-match';
import {isString} from 'util';
import {IfinalMainForm} from '../../interfaces/final-main-form';
import {MatSnackBar} from '@angular/material';
import {MatchService} from './match.service';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.sass']
})
export class MatchComponent implements OnInit {
  @Input() finalFormValues: IfinalMainForm;
  @Input() callToActionBtnAfterSubmit: boolean;

  public bestMatch: IbestMatch;
  public callToActionBtn: boolean;

  constructor(private matchResult: BackendService, private snackBar: MatSnackBar, private matchService: MatchService) {
  }

  ngOnInit() {
    let userWithTheHighestRank: number;
    let theHighestRank = 0;
    this.matchResult.listOfFoundedMatches$.subscribe(listOfFoundedMatches => {
      if (listOfFoundedMatches) {
        // Get the user with the highest relative rank
        Object.keys(listOfFoundedMatches).forEach(key => {
          if (listOfFoundedMatches[key].rank) {
            if (listOfFoundedMatches[key].rank.relative > theHighestRank) {
              userWithTheHighestRank = Number(key);
              theHighestRank = listOfFoundedMatches[key].rank.relative;
            }
          }
        });
        if (!isString(listOfFoundedMatches[userWithTheHighestRank])) {
          this.bestMatch = listOfFoundedMatches[userWithTheHighestRank];
        }
        userWithTheHighestRank = 0;
        theHighestRank = 0;
        console.log(listOfFoundedMatches);
      }
    });
  }

  /**
   * @description Send a request to the user
   */
  public sendTask() {
    this.matchService.getCall().subscribe((status) => { this.callToActionBtn = status; });
    console.log(this.callToActionBtn);

    this.finalFormValues.uid = Number(this.bestMatch.data[0].uid);
    this.matchResult.sendEmailToMatchPerson(this.finalFormValues).subscribe(data => {
      if (data === 1) {
        delete this.finalFormValues.uid;
        this.snackBar.open('Your request has been sent successfully! You may get contacted soon.', 'X', {
          panelClass: 'mat-snack-bar-success',
          duration: 7000,
          verticalPosition: 'top'
        });
        this.matchService.setCall(true);
        // this.callToActionBtn = true;
      }
    });
  }

  /**
   * @description Outputs the available info about the name
   * @param name Allowed strings: 'first name', 'last name', 'both', 'none'
   */
  public nameExists(name: string): boolean {
    if (this.bestMatch) {
      if (name === 'both') {
        if (this.bestMatch.data[0].first_name && this.bestMatch.data[0].last_name) {
          return true;
        }
      } else if (name === 'first name') {
        if (this.bestMatch.data[0].first_name && this.bestMatch.data[0].last_name === null) {
          return true;
        }
      } else if (name === 'last name') {
        if (this.bestMatch.data[0].last_name && this.bestMatch.data[0].first_name === null) {
          return true;
        }
      } else if (name === 'none') {
        if (this.bestMatch.data[0].last_name === null && this.bestMatch.data[0].first_name === null) {
          return true;
        }
      }
    }
  }

  /**
   * @description Returns the users age
   */
  public getAge(): number {
    if (this.bestMatch) {
      return new Date().getFullYear() - new Date(Number(this.bestMatch.data[0].date_of_birth)).getFullYear();
    }
  }

  /**
   * @description Returns the type of task
   */
  public getTypeOfTask(): string|null {
    if (this.bestMatch) {
      if (this.bestMatch.data[0].project === '1') {
        return 'Project';
      } else if (this.bestMatch.data[0].feature === '1') {
        return 'Feature';
      } else if (this.bestMatch.data[0].bugfix === '1') {
        return 'Bugfix';
      } else if (this.bestMatch.data[0].other === '1') {
        return 'Other';
      } else {
        return null;
      }
    }
  }

  /**
   * @description Returns the payment time
   */
  public getPaymentTime(): string|null {
    if (this.bestMatch) {
      if (this.bestMatch.data[0].before && this.bestMatch.data[0].before !== '0') {
        return 'Before';
      } else if (this.bestMatch.data[0].after && this.bestMatch.data[0].after !== '0') {
        return 'After';
      } else if (this.bestMatch.data[0].before_after && this.bestMatch.data[0].before_after !== '0') {
        return '50% before, 50% after';
      } else {
        return null;
      }
    }
  }

}
