import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MatchService {

  constructor() { }

  callToActionBtnSubject = new BehaviorSubject<boolean>(false);
  setCall(value: boolean): void {
    this.callToActionBtnSubject.next(value);
  }
  getCall(): Observable<boolean> {
    return this.callToActionBtnSubject.asObservable();
  }
}
