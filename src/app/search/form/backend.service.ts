import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Itechnologies } from '../../interfaces/technologies';
import {BehaviorSubject, Observable} from 'rxjs';
import {Imatch} from '../../interfaces/best-match';
import {IfinalMainForm} from '../../interfaces/final-main-form';
import {Iautofill} from '../../interfaces/autofill';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  private listOfFoundedMatches: BehaviorSubject<any> = new BehaviorSubject(null);
  public listOfFoundedMatches$: Observable<any> = this.listOfFoundedMatches.asObservable();

  constructor(private http: HttpClient) { }

  // Get technologies from database
  getTechnologies(value): Observable<Itechnologies[]> {
    return this.http.post<Itechnologies[]>(environment.apiBaseUrl + '/technologies', value);
  }

  // Sending email to the match person
  sendEmailToMatchPerson(data: IfinalMainForm): Observable<number> {
    return this.http.post<number>(environment.apiBaseUrl + '/send-task', data);
  }

  // Sending the form
  getMatches(values) {
    return this.http.post<Imatch>(environment.apiBaseUrl + '/match', values);
  }

  // Set list of founded matches
  setListOfFoundedMatches(value) {
    this.listOfFoundedMatches.next(value);
  }

  /**
   * @description Returns true, if there is no error
   * @param value The needed data
   */
  listOfFoundedMatchesExist(value): boolean {
    return !this.listOfFoundedMatches.hasError;
  }

  // Get list of founded matches
  getListOfFoundedMatches() {
    return this.listOfFoundedMatches.value;
  }

  /**
   * @description Checks if autofill has enough data
   * @return Returns either 1 or 0
   */
  public autoFillExists(): Observable<number> {
    return this.http.get<number>(environment.apiBaseUrl + '/autofill/has-data');
  }

  public getAutofillData(): Observable<Iautofill> {
    return this.http.get<Iautofill>(environment.apiBaseUrl + '/autofill/get-data');
  }
}
