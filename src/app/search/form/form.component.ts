import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {COMMA, ENTER, SPACE, TAB} from '@angular/cdk/keycodes';
import {
  DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE,
  MAT_DATEPICKER_VALIDATORS,
  MatAutocomplete,
  MatChipInputEvent,
  MatSelect,
  MatSnackBar
} from '@angular/material';
import {BackendService} from './backend.service';
import {SelectionService} from '../sections/selection.service';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import countries from '../../../assets/json/countries.json';
import languages from '../../../assets/json/languages.json';
import {delay} from 'rxjs/operators';
import {pipe} from 'rxjs';
import {isString} from 'util';
import {IfinalMainForm} from '../../interfaces/final-main-form';
import {IcleanForm} from '../../interfaces/clean-form';
import {MatchService} from '../match/match.service';
import {ProfileComponent} from '../../profile/profile.component';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter} from '@angular/material-moment-adapter';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}
  ]
})
export class FormComponent implements OnInit {
  @ViewChild('techInput', {static: false}) techInput: ElementRef<HTMLInputElement>;
  @ViewChild('minPrice', {static: false}) minPrice: ElementRef<HTMLInputElement>;
  @ViewChild('maxPrice', {static: false}) maxPrice: ElementRef<HTMLInputElement>;
  @ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;
  @ViewChild('paymentTimePrimary', {static: true}) paymentTimePrimary: MatSelect;
  @ViewChild('dateFrom', {static: false}) dateFrom: ElementRef<HTMLInputElement>;
  @ViewChild('dateUntil', {static: false}) dateUntil: ElementRef<HTMLInputElement>;
  @ViewChild('bestMatch', {static: false}) bestMatch: ElementRef<HTMLInputElement>;
  @ViewChild('progressBar', {static: false}) progressBar: ElementRef<HTMLInputElement>;
  @Input() section: string;

  separatorKeysCodes = [COMMA, SPACE, TAB, ENTER];
  selectable = false;
  removable = true;
  addOnBlur = true;
  technologies = [];
  setTechnologies = new Set();
  currentTechnologies = [];
  minDate = new Date();
  countries = countries;
  languages = languages;
  finalFormValues: IfinalMainForm = {};
  callToActionBtn;
  public autoFillExists: boolean;
  public autofillPercent: string;
  public endOfLoading = false;
  private lastMatch: string;

  // Primary form group
  primaryFormGroup = this.fb.group({
    title: [null, Validators.minLength(5)],
    description: [null, Validators.minLength(30)],
    gender: '',
    city: '',
    country: '',
    language: '',
    highestEducation: '',
    dateOfBirth: '',
    workload: [null, Validators.max(200)],
    typeOfTask: '',
    hourlyRate: '',
    paymentTime: '',
    before: '',
    after: '',
    before_after: '',
    minPrice: '',
    maxPrice: '',
    deadlineFrom: [null, MAT_DATEPICKER_VALIDATORS],
    deadlineUntil: [null, MAT_DATEPICKER_VALIDATORS],
    technologies: '',
    milestones: [false]
  });
  // Important form group
  importantFormGroup = this.fb.group({
    gender: [false, Validators.requiredTrue],
    city: [false, Validators.requiredTrue],
    country: [false, Validators.requiredTrue],
    language: [false, Validators.requiredTrue],
    highestEducation: [false, Validators.requiredTrue],
    dateOfBirth: [false, Validators.requiredTrue],
    workload: [false, Validators.requiredTrue],
    hourlyRate: [false, Validators.requiredTrue],
    paymentTime: [false, Validators.requiredTrue]
  });

  constructor(private backendService: BackendService, private selection: SelectionService, private fb: FormBuilder, private snackbar: MatSnackBar, private matchService: MatchService) {}

  // Form Control

  ngOnInit() {
    // Check if autofill exists
    this.backendService.autoFillExists().subscribe(autofillExists => {
      if (autofillExists === 1) {
        this.autoFillExists = true;
      }
    });

    // Set the min date
    this.minDate = new Date(this.minDate.getFullYear(), this.minDate.getMonth(), this.minDate.getDate());

    // Add new controller(s)
    this.primaryFormGroup.addControl('tech', new FormControl('', []));
    // Modify the primary form object
    this.primaryFormGroup.valueChanges.pipe(delay(500)).subscribe(inputFields => {
      if (inputFields) {
        this.importantFormGroup.updateValueAndValidity();
        // Let's remove all empty or null values
        const clean: IcleanForm = Object.keys(inputFields).filter(k => !!inputFields [k]).reduce((acc, curr) => {
          acc[curr] = inputFields[curr];
          return acc;
        }, {});

        /* Set errors */
        // minPrice
        if (this.maxPrice.nativeElement.value.length > 0) {
          if (inputFields.maxPrice < this.minPrice.nativeElement.value) {
            this.primaryFormGroup.get('maxPrice').setErrors({
              minIsGreaterThanMax: true
            });
          } else {
            this.primaryFormGroup.get('maxPrice').setErrors(null);
          }
        }
        // Deadline should not be less than start date
        if (new Date(this.dateUntil.nativeElement.value).getTime() < new Date(this.dateFrom.nativeElement.value).getTime()) {
          this.primaryFormGroup.get('deadlineUntil').setErrors({
            minDateIsGreaterThanMaxDate: true
          });
        }

        // Change technologies
        if (inputFields.technologies) {
          pipe(delay(500));
          delete clean.technologies;
          if (this.setTechnologies.size !== 0) {
            clean.tech = Array.from(this.setTechnologies);
          }
        }
        // Change age (from age to timestamp)
        if (inputFields.dateOfBirth) {
          clean.dateOfBirth = new Date (`${new Date().getFullYear() - inputFields.dateOfBirth}-01-01`).getTime() / 1000;
        }
        // Change the start and the deadline dates
        if (inputFields.deadlineFrom) {
          clean.deadlineFrom = new Date(inputFields.deadlineFrom).getTime() / 1000;
        }
        if (inputFields.deadlineUntil) {
          clean.deadlineUntil = new Date(inputFields.deadlineUntil).getTime() / 1000;
        }
        // Change paymentTime
        if (inputFields.paymentTime) {
          switch (inputFields.paymentTime) {
            case 'before':
              clean.before = 1;
              break;
            case 'after':
              clean.after = 1;
              break;
            case 'before_after':
              clean.before_after = 1;
              break;
            case 'after_milestone':
              clean.after_milestone = 1;
              break;
          }
          delete clean.paymentTime;
        }
        // Change type-of-task
        if (inputFields.typeOfTask) {
          if (inputFields.typeOfTask === 'project') {
            clean.project = 1;
          } else if (inputFields.typeOfTask === 'feature') {
            clean.feature = 1;
          } else if (inputFields.typeOfTask === 'bugfix') {
            clean.bugfix = 1;
          } else if (inputFields.typeOfTask === 'other') {
            clean.other = 1;
          }
          delete clean.typeOfTask;
        }
        // Create the request
        this.finalFormValues.request = clean;
      }
    });

    // Modify the important form object
    this.importantFormGroup.valueChanges.subscribe(inputFields => {
      if (inputFields) {
        // Create arrays of keys and values
        const importantFormGroupKeys = [];
        const importantFormGroupValues = [];
        for (const key in inputFields) {
          if (inputFields.hasOwnProperty(key)) {
            const value: string = this.importantFormGroup.get(key).value;
            if (value) {
              // paymentTime is special
              // this.primaryFormGroup.updateValueAndValidity({emitEvent: true});
              let paymentTimeKey: string|null = null;
              if (key === 'paymentTime') {
                // Check for the selected paymnetTime
                if (this.finalFormValues.request.before) {
                  paymentTimeKey = 'before';
                } else if (this.finalFormValues.request.after) {
                  paymentTimeKey = 'after';
                } else if (this.finalFormValues.request.before_after) {
                  paymentTimeKey = 'after_milestone';
                } else if (this.finalFormValues.request.after_milestone) {
                  paymentTimeKey = 'after_milestone';
                } else {
                  paymentTimeKey = null;
                }
                switch (paymentTimeKey) {
                  case 'before':
                    importantFormGroupKeys.push('before');
                    importantFormGroupValues.push(1);
                    break;
                  case 'after':
                    importantFormGroupKeys.push('after');
                    importantFormGroupValues.push(1);
                    break;
                  case 'before_after':
                    importantFormGroupKeys.push('before_after');
                    importantFormGroupValues.push(1);
                    break;
                  case 'after_milestone':
                    importantFormGroupKeys.push('after_milestone');
                    importantFormGroupValues.push(1);
                    break;
                }
              } else {
                importantFormGroupKeys.push(key);
                importantFormGroupValues.push(this.primaryFormGroup.get(key).value);
              }
            }
          }
        }
        this.finalFormValues.important = {
          columns: importantFormGroupKeys,
          values: importantFormGroupValues
        };
      }
    });
  }

  // Get the current section
  getSelectedSection() {
    return this.selection.getSection();
  }

  // On Key up, show technologies
   onKeyUp(event: any): void {
    if (event.target.value.trim().length > 0) {
      this.backendService.getTechnologies(event.target.value)
        .subscribe(data => {
          if (JSON.stringify(this.technologies) !== JSON.stringify(data)) {
            this.technologies = data;
          }
        });
    }
  }

  // On enter, add technology
  // Not needed for now
  /*
  onEnter(evt: any) {
    if (evt.source.selected) {
      this.add(evt.source);
      evt.source.value = '';
    }
  }
  */

  // Add technologies
  add(event: MatChipInputEvent): void {
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      if ((value || '').trim()) {
        // E.g., { "name": "Java" }
       //  this.currentTechnologies.push({name: value.trim()});
      }

      // Reset the input value
      if (input) {
        event.input.value = '';
      }
    }
  }

  // Select the autocomplete
  selected(): void {
    if (!JSON.stringify(this.currentTechnologies).includes(`{"name":"${this.techInput.nativeElement.value.trim()}"`)) {
      this.currentTechnologies.push({name: this.techInput.nativeElement.value.trim()});
      // Set technologies without keys
      if (this.setTechnologies) {
        this.setTechnologies.add(this.techInput.nativeElement.value);
      } else {
        this.setTechnologies = new Set();
        this.setTechnologies.add(this.techInput.nativeElement.value);
      }
      this.techInput.nativeElement.value = '';
    }
  }

  // Remove technology
  remove(tech: any): void {
    const index = this.currentTechnologies.indexOf(tech);

    if (index >= 0) {
      this.currentTechnologies.splice(index, 1);
      this.finalFormValues.request.tech.splice(index, 1);
      this.setTechnologies.delete(tech.name);
      if (this.finalFormValues.request.tech.length === 0) {
        delete this.finalFormValues.request.tech;
      }
    }
  }

  // Submitting the form
  onSubmit() {
    // There are errors
    this.endOfLoading = null;
    if (this.primaryFormGroup.status === 'INVALID') {
      this.endOfLoading = false;
      // Display the snack bar with the error message
      this.snackbar.open('You have some errors. Please check the form.', 'X', {
        panelClass: 'mat-snack-bar-error',
        duration: 5000,
        verticalPosition: 'top'
      });
      this.primaryFormGroup.markAllAsTouched();
    } else {
      this.snackbar.dismiss();
      this.backendService.getMatches(this.finalFormValues).subscribe((data) => {
        // User needs to enter more details
        if (isString(data)) {
          this.endOfLoading = false;
          // Display the snack bar with the warning message
          this.snackbar.open('No results found. Please enter more details.', 'X', {
            panelClass: 'mat-snack-bar-warning',
            duration: 5000,
            verticalPosition: 'top'
          });
        } else {
          // There is no server error
          if (this.backendService.listOfFoundedMatchesExist(data)) {
            // Let's show up the result with an animation
            if (this.lastMatch !== JSON.stringify(this.finalFormValues)) {
              this.matchService.setCall(false);
              this.animate().then();
              // this.callToActionBtn = true;
              if (window.getComputedStyle(document.querySelector('.best-match')).opacity === '1') {
                setTimeout(() => {
                  this.backendService.setListOfFoundedMatches(data);
                }, 500);
              } else {
                this.backendService.setListOfFoundedMatches(data);
              }
              this.lastMatch = JSON.stringify(this.finalFormValues);
              this.snackbar.dismiss();
            } else {
              this.snackbar.open('No changes were made.', 'X', {
                panelClass: 'mat-snack-bar-warning',
                duration: 5000,
                verticalPosition: 'top'
              });
            }
            this.endOfLoading = false;
          }
        }
      });
    }
  }

  private async animate() {
    const selector = (document.querySelector('.best-match') as HTMLInputElement);

    const disappear = () => {
      // tslint:disable-next-line:no-unused-expression
      new Promise(() => {
        if (selector.classList.contains('smooth-appear')) {
          selector.classList.add('fast-disappear');
        }
      });
    };
    const appear = () => {
      // tslint:disable-next-line:no-unused-expression
      if (selector.classList.contains('fast-disappear')) {
        // tslint:disable-next-line:no-unused-expression
        new Promise(() => {
          setTimeout(() => {
            selector.classList.remove('fast-disappear');
            (document.querySelector('.best-match') as HTMLInputElement).classList.add('smooth-appear');
          }, 500);
        });
      } else {
        (document.querySelector('.best-match') as HTMLInputElement).classList.add('smooth-appear');
      }
    };

    const run = async () => {
      await disappear();
      await appear();
    };
    await run();
  }

  /**
   * @description This function will autofill the form, based by average values which already have been given.
   */
  public autofill(): void {
    this.backendService.getAutofillData().subscribe(data => {
      // Loop through each key
      for (const key of Object.keys(data.result)) {
        // Payday
        if (data.result.before === 1) {
          this.primaryFormGroup.get('paymentTime').patchValue('before');
        } else if (data.result.after === 1) {
          this.primaryFormGroup.get('paymentTime').patchValue('after');
        } else if (data.result.before_after === 1) {
          this.primaryFormGroup.get('paymentTime').patchValue('before_after');
        } else if (data.result.afterMilestone === 1) {
          this.primaryFormGroup.get('paymentTime').patchValue('after_milestone');
        }
        // Type of task
        if (data.result.project === 1) {
          this.primaryFormGroup.get('typeOfTask').patchValue('project');
        } else if (data.result.feature === 1) {
          this.primaryFormGroup.get('typeOfTask').patchValue('feature');
        } else if (data.result.bugfix === 1) {
          this.primaryFormGroup.get('typeOfTask').patchValue('bugfix');
        } else if (data.result.other === 1) {
          this.primaryFormGroup.get('typeOfTask').patchValue('other');
        }
        // Highest education
        if (data.result.highestEducation) {
          this.primaryFormGroup.get('highestEducation').patchValue(data.result.highestEducation);
        }
        // Age
        if (data.result.dateOfBirth) {
          const age = new Date().getFullYear() - new Date(data.result.dateOfBirth * 1000).getFullYear();
          this.primaryFormGroup.get('dateOfBirth').patchValue(age.toString());
        }
        // Deadline
        /* Not able to run the code bellow. An error appears which tells that 'deadlineUntil' is not valid, even if it's empty. */
        if (data.result.deadlineFrom) {
          this.primaryFormGroup.get('deadlineFrom').patchValue(ProfileComponent._formatDate(data.result.deadlineFrom * 1000));
          if (this.primaryFormGroup.get('deadlineFrom').errors) {
            this.primaryFormGroup.get('deadlineFrom').patchValue('');
          }
        }
        if (data.result.deadlineUntil) {
          this.primaryFormGroup.get('deadlineUntil').patchValue(ProfileComponent._formatDate(data.result.deadlineUntil * 1000));
          if (this.primaryFormGroup.get('deadlineUntil').errors) {
            this.primaryFormGroup.get('deadlineUntil').patchValue('');
          }
        }

        // Technologies
        if (key === 'tech') {
          this.currentTechnologies.push({name: data.result.tech.trim()});
        } else if (this.primaryFormGroup.get(key)) {
          this.primaryFormGroup.get(key).patchValue(data.result[key]);
        }
      }
      console.log(data);
      // The probability of the match as a percent
      const autofillPercent = parseFloat(data.probability.toString()).toFixed(0) + '%';
      this.snackbar.open(`Probability of the form entries: ${autofillPercent}`, 'X', {
        panelClass: 'mat-snack-bar-success',
        verticalPosition: 'top'
      });
    });
  }

}
