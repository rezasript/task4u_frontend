import { Pipe, PipeTransform } from '@angular/core';
import { Icountries } from '../../../interfaces/countries';

@Pipe({
  name: 'country'
})
export class CountryPipe implements PipeTransform {

  transform(allCountries: Icountries[], search: string) {
    if (search) {
      return allCountries.filter((country: Icountries) => country.name.toLowerCase().includes(search));
    }
    return allCountries;
  }

}
