import { Pipe, PipeTransform } from '@angular/core';
import { Ilanguages } from '../../../interfaces/languages';

@Pipe({
  name: 'language'
})
export class LanguagePipe implements PipeTransform {

  transform(allLanguages: Ilanguages[], search: string): any {
    if (search) {
      return allLanguages.filter((language: Ilanguages) => language.name.toLowerCase().includes(search));
    }
    return allLanguages;
  }

}
