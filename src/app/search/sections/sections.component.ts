import {Component, OnInit} from '@angular/core';
import {SelectionService} from './selection.service';

@Component({
  selector: 'app-sections',
  templateUrl: './sections.component.html',
  styleUrls: ['./sections.component.sass']
})
export class SectionsComponent implements OnInit {
  public currentSection: string;

  constructor(private selectionService: SelectionService) { }

  toggleSection(section) {
    this.currentSection = section;
    this.selectionService.setSection(section);
  }

  ngOnInit() {}

}
