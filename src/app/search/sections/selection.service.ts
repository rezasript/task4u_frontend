import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SelectionService {
  private section;

  constructor() { }

  setSection(section: string): void {
    this.section = section;
  }
  getSection(): string {
    return this.section;
  }
}
