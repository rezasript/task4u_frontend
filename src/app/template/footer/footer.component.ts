import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {
  date: number;

  constructor() {
    this.date = new Date().getFullYear();
  }

  ngOnInit() {
  }

}
