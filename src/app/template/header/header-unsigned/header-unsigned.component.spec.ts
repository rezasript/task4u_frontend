import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderUnsignedComponent } from './header-unsigned.component';

describe('UnsignedComponent', () => {
  let component: HeaderUnsignedComponent;
  let fixture: ComponentFixture<HeaderUnsignedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderUnsignedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderUnsignedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
