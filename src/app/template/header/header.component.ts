import { Component, OnInit } from '@angular/core';
import {ProfileService} from '../../profile/profile.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {
  public profileImg;
  public username;
  public apiBaseUrl;

  constructor(private profileService: ProfileService) {}

  ngOnInit() {
    this.apiBaseUrl = environment.apiBaseUrl;
    this.profileService.getEntities().subscribe(user => {
      this.profileImg = user.profileImage;
      const firstName = (user.firstName) ? user.firstName.trim() : null;
      const lastName = (user.lastName) ? user.lastName.trim() : null;
      this.username = firstName + ' ' + lastName;
    });
  }

}

