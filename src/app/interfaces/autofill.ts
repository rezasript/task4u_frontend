export interface Iautofill {
  probability: number;
  result: Irequest;
}

export interface Irequest {
  title: string;
  description: string;
  gender: string;
  dateOfBirth: number;
  technologies: string;
  milestones: boolean;
  minPrice: number;
  maxPrice: number;
  deadlineFrom: number;
  deadlineUntil: number;
  project: number;
  feature: number;
  bugfix: number;
  other: number;
  hourlyRate: number;
  before: number;
  after: number;
  before_after: number;
  afterMilestone: number;
  city: string;
  country: string;
  highestEducation: string;
  languages: string;
  workload: number;
  tech: string;
}
