export interface Ilanguages {
  code: string;
  name: string;
  nativeName: string;
}
