import {IcleanForm} from './clean-form';

export interface IfinalMainForm {
  uid?: number;
  request?: Irequest | IcleanForm;
  important?: Iimportant;
}

export interface Irequest {
  tech: Array<string>;
  before: number;
  after: number;
  before_after: number;
  after_milestone: number;
}

export interface Iimportant {
  columns?: Array<string>;
  values?: Array<string>;
}
