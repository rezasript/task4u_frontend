export interface IcleanForm {
  request?: any;
  project?: number;
  feature?: number;
  bugfix?: number;
  other?: number;
  typeOfTask?: string;
  paymentTime?: any;
  before?: number;
  after?: number;
  before_after?: number;
  after_milestone?: number;
  technologies?: any;
  tech?: Array<any>;
  deadlineFrom?: number;
  deadlineUntil?: number;
  dateOfBirth?: number;
}
