export interface IprofileUpdate {
  table: string;
  column: string;
  value: any;
  type?: string;
}

export interface IprofileData {
  gender: string;
  firstName: string;
  lastName: string;
  role: string;
  city: string;
  country: string;
  dateOfBirth: number;
  profileImage: string;
  description: string;
  language: string;
  highestEducation: string;
  workload: string;
  milestones: string;
  project: string;
  feature: string;
  bugfix: string;
  other: string;
  hourlyRate: string;
  payday: string;
  technologies: string[];
}

export interface IprofileImage {
  filename: string;
}
