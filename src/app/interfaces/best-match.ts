export interface IbestMatch {
 data: Array<Imatch>;
 rank: Irank;
}

export interface Imatch {
  uid: string;
  first_name: string;
  last_name: string;
  title: string;
  description: string;
  date_of_birth: string;
  profile_image: string;
  technologies: string;
  milestones: string;
  project: string;
  feature: string;
  bugfix: string;
  other: string;
  hourly_rate: string;
  before: string;
  after: string;
  before_after: string;
  city: string;
  country: string;
  highest_education: string;
  languages: string;
  workload: string;
}

export interface Irank {
  absolute: number;
  relative: number;
}
