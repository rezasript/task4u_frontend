import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Iregister} from '../interfaces/register';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }

  /**
   * @description Register the user
   * @param data JSON that includes E-Mail and password
   */
  public register(data): Observable<Iregister> {
    return this.http.post<Iregister>(environment.apiBaseUrl + '/register', data);
  }
}
