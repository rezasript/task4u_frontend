import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {RegisterService} from './register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {
  @ViewChild('registerForm', {static: false}) registerForm: ElementRef<HTMLInputElement>;

  registerFormGroup = this.fb.group({
    email: ['', Validators.email],
    password: ['', Validators.pattern('^(?=.*?)(?=.*?[a-z])(?=.*?[0-9])(?=.*?[.#?!@$%^&*-]).{8,}$')]
  });

  constructor(private fb: FormBuilder, private registerService: RegisterService) { }

  ngOnInit() {}

  /**
   * @description Send the form
   */
  public _onSubmit() {
    // There are some errors
    if (this.registerFormGroup.status === 'INVALID') {
      this.registerFormGroup.markAllAsTouched();
    } else {
      this.registerService.register(this.registerFormGroup.value).subscribe(response => {
        if (response.email === 'error' || response.password === 'error') {
          this.registerFormGroup.markAllAsTouched();
        } else {
          // NO errors. Let's register the user
          this.registerForm.nativeElement.innerHTML = '<h1>Thank you for signing up!</h1><p>We have sent a link to your email address. Please click on it to verify your profile. If you have any problems to register, please <a href="contact">contact</a> us.</p>';
        }
      });
    }
  }

}
