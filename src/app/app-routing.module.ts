import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SearchComponent} from './search/search/search.component';
import {RegisterComponent} from './register/register.component';
import {LoginComponent} from './login/login.component';
import {LoginGuard} from './login/login.guard';
import {ProfileComponent} from './profile/profile.component';
import {HomeComponent} from './home/home.component';
import {HomeGuard} from './home/home.guard';
import {SearchGuard} from './search/search/search.guard';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [HomeGuard] },
  { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },
  { path: 'register', component: RegisterComponent, canActivate: [LoginGuard] },
  { path: 'search', component: SearchComponent, canActivate: [SearchGuard] },
  { path: 'edit-profile', component: ProfileComponent, canActivate: [SearchGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
