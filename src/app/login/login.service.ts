import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  /**
   * @description Sign in the user
   * @param data JSON that includes 'email' and 'password'
   */
  public signIn(data): Observable<number> {
    return this.http.post<number>(environment.apiBaseUrl + '/login', data);
  }

  /**
   * @description Check if the user has a cookie to login
   * @param key The generated cookie data
   */
  public getCookie(key): Observable<string> {
    return this.http.post<string>(environment.apiBaseUrl + '/login/get-cookie', key);
  }

  /**
   * @description Get user's cookie
   */
  public getCookieByUid(): Observable<string> {
    return this.http.get(environment.apiBaseUrl + '/login/get-cookie-by-uid', {responseType: 'text'});
  }

}
