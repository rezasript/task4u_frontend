import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {LoginService} from './login.service';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  loginFormGroup = this.fb.group({
    email: [null, Validators.email],
    password: [null, Validators.required],
    _remember_me: [false]
  });

  constructor(private fb: FormBuilder, private loginService: LoginService, private snackbar: MatSnackBar, private router: Router, private cookieService: CookieService) {}

  ngOnInit() {}

  /**
   * @description Submit the form
   */
  public _onSubmit() {
    if (this.loginFormGroup.status === 'INVALID') {
      this.loginFormGroup.markAllAsTouched();
    } else {
      this.loginService.signIn(this.loginFormGroup.value).subscribe(access => {
        if (access === 0) {
          this.snackbar.open('Email or password are not correct', 'X', {
            panelClass: 'mat-snack-bar-error',
            duration: 5000,
            verticalPosition: 'top'
          });
        } else {
          this.loginService.getCookieByUid().subscribe(cookie => {
            if (cookie) {
              this.cookieService.set('login', cookie.trim());
            }
          });
          this.router.navigate(['/search']).then();
        }
      });
    }
  }

}
