import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {SearchComponent} from '../search/search/search.component';
import {CookieService} from 'ngx-cookie-service';
import {LoginService} from '../login/login.service';

@Injectable({
  providedIn: 'root'
})
export class HomeGuard implements CanActivate {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): any {
    return this.hasAccess();
  }

  constructor(private searchComponent: SearchComponent, private cookieService: CookieService, private loginService: LoginService, private router: Router) { }

  hasAccess() {
    return new Promise(resolve => {
      this.loginService.getCookie(this.cookieService.get('login')).subscribe(cookieFromServer => {
        // User has no cookie
        if (cookieFromServer === '0') {
          return resolve(true);
        }
        // User has a cookie
        return resolve(this.router.createUrlTree(['/search']));
      });
    });
  }
}
