import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IprofileData, IprofileImage, IprofileUpdate} from '../interfaces/profile';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private httpClient: HttpClient) { }

  /**
   * @description Add the value into the database
   * @param data Iprofile
   */
  public update(data: IprofileUpdate): Observable<string> {
    return this.httpClient.post<string>(environment.apiBaseUrl + '/profile/update', data);
  }

  /**
   * @description Get all entities
   */
  public getEntities(): Observable<IprofileData> {
    return this.httpClient.get<IprofileData>(environment.apiBaseUrl + '/profile/output');
  }

  /**
   * @description Upload a file
   * @param fileToUpload The file that should be uploaded
   */
  public postFile(fileToUpload: File): Observable<IprofileImage> {
    const formData: FormData = new FormData();
    formData.append('profileImage', fileToUpload, fileToUpload.name);
    console.log(fileToUpload);
    return this.httpClient.post<IprofileImage>(environment.apiBaseUrl + '/profile/profile-picture', formData);
  }

}
