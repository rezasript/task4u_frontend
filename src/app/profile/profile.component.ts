import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import countries from '../../assets/json/countries.json';
import languages from '../../assets/json/languages.json';
import {COMMA, ENTER, SPACE} from '@angular/cdk/keycodes';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MAT_DATEPICKER_VALIDATORS, MatChipInputEvent} from '@angular/material';
import {ProfileService} from './profile.service';
import {IprofileUpdate} from '../interfaces/profile';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {environment} from '../../environments/environment';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}
  ],
  encapsulation: ViewEncapsulation.None
})
export class ProfileComponent implements OnInit {
  @ViewChild('gender', {static: false}) gender: ElementRef<HTMLInputElement>;
  @ViewChild('theProfileImage', {static: false}) theProfileImage: ElementRef<HTMLInputElement>;

  selectable = true;
  removable = true;
  addOnBlur = true;
  technologies: Array<string> = [];
  readonly separatorKeysCodes: number[] = [ENTER, COMMA, SPACE];
  private valueChanged: boolean;
  public countries = countries;
  public languages = languages;
  public maxDate = new Date();
  public minDate = new Date();
  public loading: string;
  public saved: string;
  public profileImageError: boolean;
  public profileFormGroup = this.fb.group({
    firstName: '',
    lastName: '',
    role: '',
    description: '',
    gender: '',
    city: '',
    country: '',
    language: '',
    highestEducation: '',
    dateOfBirth: ['', MAT_DATEPICKER_VALIDATORS],
    workload: ['', Validators.max(200)],
    project: '',
    feature: '',
    bugfix: '',
    other: '',
    hourlyRate: '',
    payday: '',
    technologies: '',
    milestones: ''
    });

  constructor(private fb: FormBuilder, private profileService: ProfileService) {}

  /**
   * @description Format the date from timestamp to a UTC string
   * @param timestamp The timestamp that should be converted
   */
  public static _formatDate(timestamp: number): string {
    const date = new Date(+timestamp);
    const offset = date.getTimezoneOffset();
    if (offset < 0) {
      date.setHours(12, 0, 0);
    }
    return date.toISOString().substring(0, 10);
  }

  ngOnInit() {
    this.minDate = new Date(this.minDate.getFullYear() - 100, this.minDate.getMonth(), this.minDate.getDate());
    this.maxDate = new Date(this.maxDate.getFullYear() - 16, this.minDate.getMonth(), this.minDate.getDate());
    this.profileService.getEntities().subscribe(data => {
      // Set default values to form fields
      this.profileFormGroup.get('gender').patchValue(data.gender);
      this.profileFormGroup.get('firstName').patchValue(data.firstName);
      this.profileFormGroup.get('lastName').patchValue(data.lastName);
      this.profileFormGroup.get('dateOfBirth').patchValue(ProfileComponent._formatDate(data.dateOfBirth));
      this.profileFormGroup.get('description').patchValue(data.description);
      this.profileFormGroup.get('role').patchValue(data.role);
      this.profileFormGroup.get('city').patchValue(data.city);
      this.profileFormGroup.get('country').patchValue(data.country);
      this.profileFormGroup.get('language').patchValue(data.language);
      this.profileFormGroup.get('highestEducation').patchValue(data.highestEducation);
      this.profileFormGroup.get('workload').patchValue(data.workload);
      this.profileFormGroup.get('project').patchValue(data.project);
      this.profileFormGroup.get('feature').patchValue(data.feature);
      this.profileFormGroup.get('bugfix').patchValue(data.bugfix);
      this.profileFormGroup.get('other').patchValue(data.other);
      this.profileFormGroup.get('milestones').patchValue(data.milestones);
      this.profileFormGroup.get('hourlyRate').patchValue(data.hourlyRate);
      this.profileFormGroup.get('payday').patchValue(data.payday);
      // Set profile picture
      if (data.profileImage !== null) {
          const profileImagePath = `${environment.apiBaseUrl}/uploads/profile-images/${data.profileImage}`;
          this._urlExists(profileImagePath, (fileExists) => {
            if (fileExists) {
              this.theProfileImage.nativeElement.src = `${environment.apiBaseUrl}/uploads/profile-images/${data.profileImage}`;
            }
          });
      }
      for (const technology of data.technologies) {
        this.technologies.push(technology);
      }
    });

    // Check if there are any changes
    Object.keys(this.profileFormGroup.controls).forEach(key => {
      this.profileFormGroup.controls[key].valueChanges.subscribe(_ => {
        if (!this.profileFormGroup.get(key).pristine) {
          this.valueChanged = true;
        }
      });
    });
  }

  // Add technology
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.technologies.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  // Remove technology
  remove(technology: string): void {
    const index = this.technologies.indexOf(technology);

    if (index >= 0) {
      this.technologies.splice(index, 1);
      this.valueChanged = true;
      this.update('technologies', 'tech', this.technologies.toString());
    }
  }

  /**
   * @description Add the value into the database
   * @param tableName The table of the database
   * @param columnName The column of the database
   * @param theValue The value that should be inserted/updated
   */
  public update(tableName: string, columnName: string, theValue: any) {
    // Only if the value has been changed
    if (this.valueChanged) {
      this.valueChanged = false;
      this.loading = columnName;

      // Set the correct value of dateOfBirth
      if (columnName === 'dateOfBirth') {
        const splitDate = theValue.split(/\s|,\s/);
        const newDate = `${splitDate[2]}/${splitDate[0]}/${splitDate[1]}`;
        theValue = new Date(newDate).getTime();
      }

      const json = {
        table: tableName,
        column: columnName,
        value: theValue
      };
      // Add 'type' to JSON, if the table is 'languages' or 'payment'
      if (tableName === 'languages' || tableName === 'payment') {
        (json as IprofileUpdate).type = 'skill';
      }

      // Update the value
      this.profileService.update(json).subscribe(_ => {
        this.loading = '';
        this.saved = columnName;
        setTimeout(() => {
          this.saved = '';
        }, 2000);
      });
    }
  }

  /**
   * @description Uploads the profile image
   * @param event Event of the file that should be uploaded
   */
  public uploadImage(event): void {
    const profileImage = event.files.item(0);
    this.profileService.postFile(profileImage).subscribe(data => {
      if (data.filename === '0') {
        this.profileImageError = true;
      } else if (data.filename) {
        this.profileImageError = false;
        this.theProfileImage.nativeElement.src = `${environment.apiBaseUrl}/uploads/profile-images/${data.filename}`;
      }
    });
  }

  private _urlExists(url: string, callback) {
    fetch(url, { method: 'head' })
      .then((status) => {
        callback(status.ok);
      });
  }
}
